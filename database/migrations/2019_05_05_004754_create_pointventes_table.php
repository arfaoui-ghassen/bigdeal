<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointventesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pointventes', function (Blueprint $table) {
            $table->Increments('id');
            $table->String('adresse');
            $table->unsignedInteger('marchand_id') ;
            $table->foreign('marchand_id')->references('id')->on('marchands')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pointventes');
    }
}
