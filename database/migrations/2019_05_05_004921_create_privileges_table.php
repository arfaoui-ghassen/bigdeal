<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivilegesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privileges', function (Blueprint $table) {
            $table->Increments('id');
            $table->String('nompriv');
            $table->text('descrip');
            $table->Float('remise');
            $table->Date('datedep');
            $table->Date('datefin');
            $table->unsignedInteger('marchand_id');
            $table->foreign('marchand_id')->references('id')->on('marchands')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privileges');
    }
}
