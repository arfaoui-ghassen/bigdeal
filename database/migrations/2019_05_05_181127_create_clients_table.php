<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('nom');
            $table->String('prenom');
            $table->String('adresse');
            $table->String('email');
            $table->date('date_nais');
            $table->bigInteger('tel');
            $table->integer('codepost');
            $table->unsignedBigInteger('carte_id');
            $table->foreign('carte_id')->references('id')->on('cartes')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
