<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('homepage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('client', 'ClientController');
Route::resource('privilege', 'PrivilegeController');
Route::resource('abonnement', 'CarteController');
Route::resource('marchand', 'MarchandController');
Route::get('/Acceuil', function() {
        return view ('frontoffice.Acceuil');
});
Route::get('/Propos', function() {
    return view ('frontoffice.Propos');
});
Route::get('/editc/{$id}', function() {
    return view ('frontoffice.editc');
});
Route::get('/clientAcc', 'ClientController@clienthome');
Route::get('/Acceuil', function() {
    return view ('frontoffice.Acceuil');
});
Route::get('/Ajoutprivi', function() {
    return view ('frontoffice.Ajoutprivi');
});


Route::get('/createc','ClientController@create');
Route::get('/createm','MarchandController@create');

Route::POST('/client/update/{id}','ClientController@updateClient')->name('update_client');
Route::POST('/client/home','ClientController@loginuser')->name('loginuser');
Route::get('/logoutclient','ClientController@logout')->name('logoutclient');
Route::get('/client','ClientController@index') ;
Route::get('/logasclient',function(){
    return view ('frontoffice.Client');
}) ;

Route::get('/marchandAcc', 'MarchandController@marchand_acceuil');
Route::get('/logasmarchand','MarchandController@logininter');
Route::get('/marchand/ajout/acheteur/{id}','MarchandController@ajoutacheteur')->name('_ajoutacheteur');
Route::get('/logoutmarchand','MarchandController@logout')->name('logoutmarchand');
Route::POST('/logasmarchand','MarchandController@login')->name('login_marchand') ;
Route::POST('/marchand/ajout/privilege','MarchandController@ajoutprivilege')->name('ajoutprivilege');
Route::POST('/marchand/add/priv','PrivilegeController@addpriv')->name('addpriv');
Route::POST('/marchand/update','MarchandController@update')->name('update_marchand');

    // Route::get('/editm', function() {
    
    //     return view ('frontoffice.editm');
    // });
Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    Route::get('dashboard', function() {} );
});
