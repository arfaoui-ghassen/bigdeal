@extends('frontoffice.acc')
@section('contents')

@if (\Session::has('error'))
      <div class="alert alert-danger">
        <p>{{ \Session::get('error') }}</p>
      </div><br />

     @endif
@if (\Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif


<div class ="container">
      <br>
      <br>

    <img src="https://club.bigdeal.tn/frontend/bigdeal/images/1.png" class="rounded mx-auto d-block">
                  <br>

                 <br>
           <div class="section-title">
                <h2 class="text-primary">  Connexion  au Club:  </h2>
                     <p>
                        <form method="POST" action="{{ route('loginuser') }}" >
                          @csrf
                                <div class="form-group">
                                  <label class="required"> Saisir ID du carte : </label>
                                  <input name="carte_id" type="text"   placeholder="8 caractères ">
                                  <small id="emailHelp" class="form-text text-muted">( champ obligatoire *) .</small>
                                </div>
                                <input type="submit"  class="btn btn-warning"  value="Login">
                        </form>
                      </p>
            </div>
            <br>
           <hr align="center" width="100%">
           <div class="section-title" align="center">
                            <h2  class="text-success"> Comment Adhérer au club !</h2>
                     <p class="text-secondary">Pour vous inscrire au Club Privilèges vous n’avez qu’à remplir le formulaire ci-dessous.<br>
               vous recevrez votre carte membre à l'adresse mentionnée dans les 48 heures.
                     <br> <p><a href="{{url('createc')}}" class="text-primary"><b><h5> Inscriver-vous ICI  </h5></b></a></p>
                      </p>


            </div>

</div></div>

@endsection
