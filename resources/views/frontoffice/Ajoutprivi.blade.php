@extends('frontoffice.acc')
@section('contents')
<div class="container" >
    <br>
    <br>

<h4 class="text-info"> <u> Ajouter un Privilège </u> </h4>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>
<form method="POST" action="{{route('addpriv')}}">
    @csrf
    <div class="form-row">
      <div class="form-group col-md-6">
        <label> Nom <abbr class="required">*</abbr> </label>
        <input type="nom" class="form-control" name="nompriv" placeholder="Nom ">
      </div>
      <div class="form-group col-md-6">
        <label> Remise  <abbr class="required">*</abbr></label>
        <input type="decimal" class="form-control" name="remise" placeholder="Remise">
      </div>
    </div>
    <div class="form-group">
            <label>Description du privilège <abbr class="required">*</abbr></label>
      <textarea type="text" class="form-control" name="descrip" placeholder="Toutes les dicription du priviléges"> </textarea>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label>Date-Début <abbr class="required">*</abbr></label>
        <input type="date" class="form-control" name="datedep"min="2019-01-01" max="2050-12-31">
      </div>
      <div class="form-group col-md-4">
        <label>Date-Fin <abbr class="required">*</abbr> </label>
        <input type="date" class="form-control" name="datefin" min="2019-01-01" max="2050-12-31">
      </div>
    </div >
          <br>
           <br>
           <div class="form-row">
           <div class="form-group col-md-6" align="center">
                <br>
                <button  type="submit" class="btn btn-info btn-lg btn-block">  Ajouter un privilège  </button> </a>
           </div>
           </div>

    <br>
    <br>
  </form>
</div>
@endsection
