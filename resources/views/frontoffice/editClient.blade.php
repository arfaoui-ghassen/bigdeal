@extends('frontoffice.acc')
@section('contents')
<div class="container" >
    <br>
    <br>

<h4 class="text-info"> <u> Inscription au Club </u> </h4>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>
<form  action="{{ route('update_client', $client->id) }}" method="POST">
        {{ csrf_field() }}
    <div class="form-row">
      <div class="form-group col-md-6">
        <label> Nom <abbr class="required">*</abbr> </label>
           <input type="text" name="nom" value="{{ isset($client->nom) ? $client->nom : null }}" class="form-control">
      </div>
      <div class="form-group col-md-6">
        <label>Prénom <abbr class="required">*</abbr></label>
        <input type="text" class="form-control" name="prenom" value="{{ isset($client->prenom) ? $client->prenom : null }}">
      </div>
    </div>
    <div class="form-group">
      <label>Email <abbr class="required">*</abbr></label>
      
      <input type="text" class="form-control" name="email" value="{{ isset($client->email) ? $client->email : 'foulenbenFoulen@mail.com'}}">
    </div>
    <div class="fm-group">
      <label>Addresse</label>
      <input type="text" class="form-control" name="adresse" value="{{ isset($client->adresse) ? $client->adresse : 'Saisir votre adresse'}}" >
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label>Code Postal <abbr class="required">*</abbr></label>
        <input type="number" class="form-control" name="codepost" value="{{ isset($client->codepost) ? $client->codepost: 'Saisir votre codepost'}}">
      </div>
      <div class="form-group col-md-4">
        <label>Date-Naissance <abbr class="required">*</abbr> </label>
        <input type="date" class="form-control" name="date_nais" min="1900-01-01" max="2019-12-31" value="{{ isset($client->date_nais) ? $client->date_nais: 'Saisir votre date_nais'}}" >
      </div>
      <div class="form-group col-md-2">
        <label>Télephone<abbr class="required">*</abbr></label>
        <input type="number" class="form-control" name="tel" value="{{ isset($client->tel) ? $client->tel: 'Saisir votre telephone'}}">
      </div>
    </div>

            <div class="form-group">
            <div class="form-group col-md-6">
              <h4 class="text-info"> Modes de Paiement </h4>
            </div>
            <hr>
           </div>
            <div class="form-row">
                   <div class="form-group col-md-6" >
                       <br>
                    <h5 class="text-success" ><u> Paimenet par Carte  </u></h5>
                  </div>
                  <div class="form-group col-md-4">
                    <label> Numéro du carte <abbr class="required">*</abbr> </label>
                    <input type="number" class="form-control">
                  </div>
                  <div class="form-group col-md-2">
                    <label> Code-Carte <abbr class="required">*</abbr> </label>
                    <input type="number" class="form-control" >
                  </div>
             </div>
             <div class="form-row">
                    <div class="form-group col-md-6" >
                        <br>
                     <h5 class="text-success" ><u> Ou bien Inscription Paimenet par nos PAYBOX  </u></h5>
                   </div>
             </div>
           <br>
           <br>
           <div class="form-row">
           <div class="form-group col-md-6" align="center">
                <br>
                <input type="submit" class="btn btn-warning btn-lg btn-block" value="Modifier profile">
           </div>
           </div>

    <br>
    <br>
  </form>
</div>
@endsection
