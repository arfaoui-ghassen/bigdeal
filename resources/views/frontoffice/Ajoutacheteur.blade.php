@extends('frontoffice.acc')
@section('contents')

<div class="container" >
    <br>
    <br>

<h4 class="text-info"> <u> Ajouter un Acheteur </u> </h4>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>
<form  action="{{route('ajoutprivilege')}}" method="POST">
@csrf
    <div class="form-row">
      <div class="form-group col-md-6">
        <label> ID Carte  <abbr class="required">*</abbr> </label>
        <input type="number" class="form-control" name="carte_id" placeholder=" Identifiant du client ">
      </div>
      <div class="form-group col-md-6">
        <label> Nom et Prénom <abbr class="required">*</abbr></label>
        <input type="text" class="form-control" name="nom" placeholder=" Nom & prénom ">
      </div>
    </div>
    
    <div class="form-group">
        <label> ID privilège <abbr class="">*</abbr></label>
          <select name="privilege_id" class="custom-select">
          @if(empty($marchand->privilege->all()))
              <option selected disabled >You dont have any offers yet</option>
          @else
            @foreach($marchand->privilege as $key=>$value)
              <option selected value="{{$value->id}}">{{$value->id}}</option>
            @endforeach
          @endif
        </select>
    </div>
    
    <div class="form-row">
      <div class="form-group col-md-6">
        <label> date d'achat <abbr class="">*</abbr></label>
        <input type="date" class="form-control" name="created_at"min="2019-01-01" max="2050-12-31">
      </div>

      <div class="form-group col-md-6">
        <label>Adresse <abbr class="required">*</abbr> </label>
        <input type="String" class="form-control" name="Adresse" >
      </div>
    </div>

          <br>
           <br>
           <div class="form-row">
           <div class="form-group col-md-6" align="center">
                <br>
                <button  type="submit" class="btn btn-warning btn-lg btn-block">  Ajouter un Acheteur </button> </a>
           </div>
           </div>

    <br>
    <br>
  </form>
</div>
@endsection
