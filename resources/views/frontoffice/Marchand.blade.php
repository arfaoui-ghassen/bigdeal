@extends('frontoffice.acc')
@section('contents')




<div class ="container">
  @if (\Session::has('errors'))

    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <div class="alert alert-danger" role="alert">
          {{ session('errors') }}
        </div>
    </div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
      <br>
      <br>

    <img src="https://club.bigdeal.tn/frontend/bigdeal/images/1.png" class="rounded mx-auto d-block">
                  <br>

                 <br>
           <div class="section-title">
                <h3 class="text-info">  Adhérer au Club:  </h3>

                     <p>
                            <form action = "{{route('login_marchand')}}" method="POST" >
                              @csrf
                                    <div class="form-group">
                                      <label>E-mail  </label>
                                      <input type="email" class="form-control" name="email"  placeholder="foulenbenfoulen@mail.com">
                                      <small  class="form-text text-muted">saisir votre email.</small>
                                    </div>
                                    <div class="form-group">
                                      <label> Mot de passe </label>
                                      <input type="password" class="form-control" name="password" placeholder="Mot de passe">
                                    </div>
                                    <div class="form-group form-check">
                                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                      <label class="form-check-label" for="exampleCheck1"> enregistrer le mot de passe </label>
                                    </div>
                                    <input type="submit" class="btn btn-warning" value="Connexion ">  
                                  </form>
                      </p>
            </div>
            <br>
           <hr align="center" width="100%">
           <div class="section-title" align="center">
                            <h2  class="text-success"> Comment Adhérer au club !</h2>
                     <p class="text-secondary">Pour vous inscrire au Club Privilèges vous n’avez qu’à remplir le formulaire ci-dessous.<br>
               vous pouvez consulter et publier les Privilèges .
                     <br> <p><a href="{{url('createm')}}"> <b class="text-primary"> Inscriver-Vous </b></a></p>
                      </p>


            </div>

</div></div>

@endsection
