@extends('frontoffice.acc')
@section('contents')

<div class="container"></div>

<div class="clearfix space"></div>
        <section  class="our-contact section-big pdt">
        <div class="container">
            <div class="row">
                <div class="section-title" align="center">
                    <h2 align="center" class="text-info">Qui sommes-nous</h2>
                </div>
			</div>



	<div class="condition"><p><b class="text-primary"> À propos de B!GDeal
</b></p><p>Nous sommes une startup tunisienne lancée en mai 2012 par notre fondateur Issam Essefi, notre mission est de mettre à disposition de nos Clients le savoir faire et la technologie  adéquate afin de proposer de bons plans et de les rendre accessibles à tous !</p><p>
</p><p>En janvier 2018, b!gdeal a rejoint le groupe Medianet, leader en ingénierie informatique et web service créé en 1998, propulsé par Africinvest, b!gdeal s'est lancé en avril 2019 en côte d'ivoire (www.bigdeal.ci)</p><p>Nous employons plus de 20 personnes à Tunis et à Abidjan  et nous comptons aller plus loin.</p><p>Bigdeal S.A est membre du syndicat du e-commerce et de la vente à distance <a href="http://www.sevad.tn/" target="_blank">(SEVAD)</a></p><p>
</p><p ><b class="text-primary">À propos du club privilèges by BIGDeal
</b></p><p>Club privilèges by b!gdeal est un abonnement donnant droit à ses membres de profiter d’avantages&nbsp; et de remises exclusifs dans l’ensemble du réseau des enseignes participantes.
</p><p>L’application gratuite Club privilèges by bigdeal permet à ses utilisateurs de profiter des avantages exclusifs spécialement négociés auprès des enseignes de proximités.</p><p>Plus de 100 enseignes sont déjà partenaires du club privilèges by bigdeal : Fatales, gourmandise, California Gym, L'occitane de province,  des restaurants, des hôtels, des agences de voyage et bien d'autres...<br>Chaque semaine, des dizaines de nouvelles enseignes nous rejoignent.
</p><p><br></p><p><br></p></div>




        </div>
    </section>


    </div>

@endsection
