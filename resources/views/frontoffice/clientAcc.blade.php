@extends('frontoffice.acc')
@section('contents')
@if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
        <?php
        session()->forget('success') ;
        ?>
      </div><br />

     @endif
<br>
 <!--  contient la liste des privileges activee     -->
<div class="container">
        <img src="image/beaute.jpg" class="rounded float-left" alt="tof" width="200" height="200">
         <img src="image/crepe.jpg" class="rounded float-right" alt="tof" width="200" height="200">
         <br>
         <br>
          <h3 align="center" class="text-info "> <u>  Les Priviléges disponible  </u></h3>
<br>
<br>
<br>
  <table class="table table-hover">
  <br>
          <thead>
            <tr  class="table-succes">
              <th scoop="col"> Nom Privilége  </th>
              <th scope="col">Description </th>
              <th scope="col">Remise </th>
              <th scope="col">Date de Debut </th>
              <th scope="col">Date de Fin </th>
              <th scope="col">Marchand  </th>
            </tr>
          </thead>

               
          <tbody>
          @if(!is_null($priv_list))
            @foreach($priv_list as $key=>$value)
              <tr>
                <td>{{$value->nompriv}}</td>  
                <td>{{$value->descrip   }}</td>  
                <td>{{$value->remise  }}</td>  
                <td>{{$value->datedep }}</td>  
                <td>{{$value->datefin  }}</td> 
                <td>{{$value->marchand->nom  }}</td> 
              </tr> 
            @endforeach
          
        </tbody>

  </table>

@else
  <div class="alert alert-warning" role="alert" style="margin-top: 10px">
          <strong>Vous n'avez pas aucun privilege pour l'instant </strong>
  </div>  
@endif
<br>
<div class="alert alert-info" role="alert">
        Si Voulez modifier votre compte ! Cliquer Aussi  <a href="{{route('client.edit',$client->id)}}" class="alert-link"> Modifier</a>
</div>
<hr>
<a href="{{ route('logoutclient') }}" class="btn btn-warning" style="margin-bottom: 10px"> <u> Deconnexion </u></a>
<br>
</div>
@endsection
