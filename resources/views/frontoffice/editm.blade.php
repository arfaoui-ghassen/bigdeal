<!-- <?php 
?> -->
@extends('frontoffice.acc')
@section('contents')

<div class="container" >
    <br>
    <br>

<h4 class="text-info"> <u> Changer votre Informations </u> </h4>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>

<form  method="post" action="{{ route('update_marchand') }}">
  @csrf
    <div class="form-row">
      <div class="form-group col-md-6">
        <label> Nom du commercant <abbr class="required">*</abbr> </label>
        <input type="text" name="nom" class="form-control" value="{{ isset($marchand->nom) ? $marchand->nom : 'Nom'   }}" >
      </div>
      <div class="form-group col-md-4">
            <label>Télephone</label>
            <input type="text" name="tel" class="form-control" value="{{ isset($marchand->tel) ? $marchand->tel : 'tel'   }}">
      </div>
    </div>
    <div class="form-row">
            <label> E-mail <abbr class="required">*</abbr></label>
            <input type="text" class="form-control" name="email" value="{{ isset($marchand->email) ? $marchand->email : 'email'   }}">

    </div>
    <br>
        <div class="form-group">
          <label>Sélectionner les Point-ventes</label>
           <select class="custom-select" name="point_ventes"multiple>
            <!-- <option selected disabled ></option> -->
            <option value="Ariana">Ariana</option>
            <option value="Bizerte">Bizerte</option>
            <option value="Ben arous">Ben arous</option>
            <option value="Tunis">Tunis</option>
            <option value="Jandouba">Jandouba</option>
            <option value="kef">kef</option>

          </select>
        </div>
        <h4 class="text-warninig"> Changer le mot passe </h4>
        <br width=100%>
        <div class="form-group">
        <label> Mot de Passe  <abbr class="required">*</abbr> </label>
        <input type="password" class="form-control" name="motpass" placeholder="************" >
      </div>

    </div>

      <input type="submit" class="btn btn-info" value="Modifier "> 
      </div>
</form>
</div>

@endsection
