<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset="UTF-8">
        <title> Club-Priviléges </title>
    </head>
<!-- Fonts -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css" media="all" >
       <link rel="stylesheet" href="/public/css/style.css" media="all">
       <link rel="stylesheet" href="/public/css/material-design-iconic-font.min.css" media="all">
       <link rel="stylesheet" href="/public/css/font-awesome.min.css" media="all">
<!-- style -->
   <style>
              html, body {
                background-color: #fff;
                color: #0f0f0f;
                font-family: 'Lato';
                font-weight: 300;
                height: 100vh;
                font-size:large;
                margin: 0;}

             content {
                text-align: center;
                      }

             title {
                font-size: 84px;
                       }
            .navbar{

               background-color:darkslateblue;   font-weight:10cm ;
                height: 2cm;
                font-family: 'lato';
                font-size: 120%;
                ;}
            footer{
                background-color:darkslateblue;
                font-weight:10cm ;
                height: 4cm;

                  }
            :link{
                color: #fff
            }
            .no-gutters {
  margin-right: 0;
  margin-left: 0;}




   </style>
    <body >
            <marquee scrollamount="5"> Service clients <b> 70 242 800 </b>6J/7 de 9H00 à 17H </marquee>

        <!--  création de navbar  -->
            <nav  id="navbar" class="navbar navbar-expand-md navbar-light navbar-laravel" >
                    <div class="container">
                        <a class="navbar-brand" href="{{ url('Acceuil') }}"> <img src="{{url('image/logo.svg')}}" width="280"
                            height="130" >       <img src="{{url('image/ccc.png')}}" width="90"
                            height= "40"> </a>

                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="nav navbar-nav" id="navbarSupportedContent">

                         <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                        <a class="nav-link active" href="{{url('/Acceuil')}}"><u>{{ __('Acceuil ') }}</u></a>
                                      </li>
                                <li class="nav-item">
                                 <a class="nav-link active" href="{{url('/Client')}}"><u>{{ __('Client ') }}</u> </a>
                                </li>

                                <li class="nav-item">
                                  <a class="nav-link active" href="{{url('/Marchand')}}"><u>{{ __('Marchand ') }}</u></a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link active" href="{{url('/Propos')}}"> <u> {{ __('A propos de ') }} </u></a>
                                 </li>


                          </ul>

                       </div>
            </nav>
            </div>

        </header>

        @yield('contents')
        <!--  lien de JavaScript et bootstrap    -->
<script src={{"assets ('assets/js/jquery1.min.js"}} ></script>
<script src={{"assets ('assets/js/bootstrap.min.js)"}} ></script>

        <footer id="footer" class="row row-bottom-footer">

                <div class="container">
                    <div class="example-containe">

                            <div class="example-row">
                                    <div class="example-content-main">
                            <h4>Contactez nous</h4>
                                        <ul>
                                            <li><a href="tel:+21671242802">Tél: +216 71 242 802</a></li>
                                            <li><a href="mailto:club@bigdeal.tn">Mail: Club@bigdeal.tn</a></li>
                                        </ul>
                                    </div>
                            </div>

                           <div class="example-content-secondary">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="image/logo.svg" height="30"width="250">
                                 <b>Nous acceptons</b>, modalités de paiements
                                 <span><img src="image/modalite-paiement.png" height="24"
                                                    width="203"></span>
                            </div>
                    </div>

<br>
<br>
                  <div class="no-gutters ">

                         <div class=".col-6 .col-md-4">
                          <div class="col-sm-8" style="text-align:right">
                                  <p>Copyright &copy; 2019. Powered by <a href="https://www.shiftyourdigital.tn" target="_blank">Shift</a></p>
                          </div>
                        </div>

                 </div>



                </div>
                </div>
        </footer>

    </body>
    </html>
