@extends('frontoffice.acc')
@section('contents')

<div class="container" >
    <br>
    <br>

<h4 class="text-info"> <u> Inscription au Club </u> </h4>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>
<form  action="{{route('marchand.store')}}" method="post">
        @csrf
    <div class="form-row">
      <div class="form-group col-md-6">
        <label> Nom du commercant <abbr class="required">*</abbr> </label>
        <input type="nom" class="form-control" name="nom" placeholder="Nom ">
      </div>
      <div class="form-group col-md-4">
            <label>Télephone</label>
            <input type="number" class="form-control" name="tel" placeholder="Tél">
      </div>
    </div>
    <div class="form-row">
            <label> E-mail <abbr class="required">*</abbr></label>
            <input type="email" class="form-control" name="email" placeholder="Email">

    </div>
        <div class="form-group">

                <label> Liste des Adresse  <abbr class="required">*</abbr></label>
           <select class="custom-select" name="point_vente[]"  multiple="multiple">
            <option selected disabled >Sélectionner les Point-ventes</option>
            <option value="Ariana">Ariana</option>
            <option value="Bizerte">Bizerte</option>
            <option value="Ben arous">Ben arous</option>
            <option value="Tunis">Tunis</option>
            <option value="Jandouba">Jandouba</option>
            <option value="kef">kef</option>
          </select>
        </div>
        <hr width=100%>
        <div class="form-group">
                <div class="form-group col-md-4">
        <label> Mot de Passe  <abbr class="required">*</abbr> </label>
        <input type="password" class="form-control" name="motpass" >
      </div>
       <div class="form-group col-md-4">
        <label> Confirm mot de passe <abbr class="required">*</abbr></label>
        <input type="password" class="form-control" name="motpass">
      </div>
    </div>

            <br>
           <br>
           <div class="form-row">
                <br>
                <a href="{{url('marchandAcc')}}"> <button  type="submit" class="btn btn-warning btn-lg btn-block"> Inscription </button> </a>
           </div>
           </div>

    <br>
    <br>
  </form>
</div>

@endsection
