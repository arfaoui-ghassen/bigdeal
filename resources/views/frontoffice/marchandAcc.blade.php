@extends('frontoffice.acc')
@section('contents')

<?php 
if (session()->has('marchand')){
  $marchand_id=session('marchand');
} 
else { ?>
  window.location.href={{url('/logasmarchand')}};  
<?php
}
?>
<br>
 <!--  contient la liste des  acheteur   -->
<div class="container">
@if (\Session::has('errors'))

    <div class="alert alert-danger">
        <strong>Whoops!</strong> Something went wrong<br>
        <div class="alert alert-danger" role="alert">
          {{ session('errors') }}
        </div>
    </div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif

        <img src="image/march.png" class="img-fluid" alt="Responsive image" height="100">
         <br>
         <br>
         <div class="form-row">
         <div class="form-group col-md-6">
          <h3 class="text-info "> <u>  Liste de mes  Acheteur   </u></h3>
     </div>
     <div class="form-group col-md-4">

<a href="{{url('marchand/ajout/acheteur/'.$marchand_id)}}" class="btn btn-outline-danger"> Ajouter Des Acheteurs </a>
</div>
 <div class="form-group col-md-2">
<a href="{{url('/Ajoutprivi')}}" ><button type="button" class="btn btn-outline-warning"> Ajouter  un Privilége </button></a>
</div>
</div>

<br>
<br>
<table class="table table-hover">
        <thead>
          <tr  class="table-succes">
            <th scoop="col">  Id carte </th>
            <th scope="col">Id-Privilege </th>
            <th scope="col">Adresse </th>
            <th scope="col">Date d'achat </th>
          </tr>
        </thead>

        <tbody>
          @if(!is_null($client_list))
            @foreach($client_list as $key=>$value)
              <tr>
                <td>{{$value->client_id}}</td>  
                <td>{{$value->privilege_id  }}</td>  
                <td>{{$value->Adresse }}</td>  
                <td>{{$value->created_at }}</td> 
              </tr> 
            @endforeach
          @endif
          
        </tbody>
</table>
<br>
<div class="alert alert-warning" role="alert">
        Si Voulez modifier votre compte ! Cliquer Aussi  <a href="{{route('marchand.show',$marchand_id)}}" class="alert-link"> Mofidier le Compte  </a>.
      </div>
<hr>
<!-- <a type=" button" " class="btn btn-light"> <u> Deconnexion </u></a> -->
<a href="{{ route('logoutmarchand') }}"  class="btn btn-primary"> Deconnexion </a>


<br>
</div>
@endsection
