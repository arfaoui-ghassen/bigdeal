@extends('backoffice.master')

@section('content')
<div class="container">
    <br />
    <h2> Gestion des Privileges</h2>
    @if (\Session::has('activepriv'))
      <div class="alert alert-success">
        <p>{{ \Session::get('activepriv') }}</p>
        @php
        Session::forget('activepriv');
        @endphp
      </div><br/>
      @endif

      @if($privileges->isEmpty())
      <div class="alert alert-warning" role="alert">
           Pas de Priviléges  !
         </div>
      @else
       <br>
     <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th> Nom</th>
        <th>Description</th>
        <th>Remise</th>
        <th>Date-Activer</th>
        <th>Date-Désac</th>
        <th>ID-marchand</th>
        <th>Date-Création</th>
        <th>Opération </th>

      </tr>
    </thead>
    <tbody>
        @foreach($privileges as $privilege)
       <tr>
           <td>{{ $privilege->id }} </td>
           <td>{{ $privilege->nompriv }}</td>
           <td>{{ $privilege->descrip }}</td>
           <td>{{ $privilege->remise}}</td>
           <td>{{ $privilege->datedep }}</td>
           <td>{{ $privilege->datefin}}</td>
           <td>{{ $privilege->marchand_id }}</td>
           <td>{{ $privilege->created_at }}</td>

           <td>
              @if($privilege->status!=0)
                <a href="{{action('PrivilegeController@show', $privilege['id'])}}" class="btn btn-danger">Desactiver</a>
              @else
                <a href="{{action('PrivilegeController@show', $privilege['id'])}}" class="btn btn-success">Activer</a>

              @endif

        </tr>
        @endforeach
@endif
    </tbody>
  </table>
  </div>
@endsection
