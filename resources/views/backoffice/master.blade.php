<!DOCTYPE html> 
<html lang='en'>
    <head> 
        <meta charset="UTF-8">
        <title> Backoffice</title>
    </head>
<!-- Fonts -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css" media="all" >
       <link rel="stylesheet" href="/public/css/style.css" media="all">
<!-- style -->
   <style> 
              html, body {
                background-color: #fff;
                color: #0f0f0f;
                font-family: 'Lato';
                font-weight: 300;
                height: 100vh;
                margin: 0;}

             .content {
                text-align: center;
                      }

             .title {
                font-size: 84px;
                       }
             .navbar{
            
                background-color:darkslateblue;   font-weight:10cm ;
                height: 2cm;
                font-family: 'lato';
                font-size: 120% ; 
                color: #fff; }
        
                     
   </style>
    <body >  
        <!--  création de navbar  -->
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel" >
                    <div class="container">
                        <a class="navbar-brand" href="{{ url('/') }}"> <img src="{{url('image/bigdeal.png')}}" width=140
                            height=39    > </a>
                           
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon"></span>
                        </button>
        
                        <div class="nav navbar-nav" id="navbarSupportedContent">
                           
                         <ul class="navbar-nav ml-auto" >
                         
                                <li class="nav-item">
                                 <a class="nav-link" href="{{url('/client')}}">{{ __('Client ') }} </a>
                                </li>
                              
                                <li class="nav-item">
                                  <a class="nav-link" href="{{url('/marchand')}}">{{ __('Marchand ') }}</a>
                                </li>

                                <li class="nav-item">
                                   <a class="nav-link" href="{{url('/privilege')}}">{{ __('Privilège ') }}</a>
                                 </li> 

                                <li class="nav-item">
                                   <a class="nav-link" href="{{url('/abonnement')}}">{{ __('Abonnement ') }}</a>
                                 </li>
                                
                                 
                                 <li class="nav-item">
                                    <a class="nav-link" href="{{ route('logout') }}"> <u> {{ __('Déconnexion  ') }}  </u></a>
                                  </li>

                          </ul>
                             
  
                                
                             
                    
                    </nav>
               
                </div>

            </div>
            
            
            
   

    @yield('content') 
                  <!--  lien de JavaScript et bootstrap    -->
       <script src={{"assets ('assets/js//jquery1.min.js"}} ></script>
       <script src={{"assets ('assets/js//bootstrap.min.js)"}} ></script>

    </body>
    </html> 
