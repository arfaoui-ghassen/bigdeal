@extends('backoffice.master')

@section('content')
<div class="container">
    <br />
    <h2> Gestion des marchands </h2>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
     @if($marchands->isEmpty())
     <div class="alert alert-warning" role="alert">
          Pas de Marchands !
        </div>
     @else
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th> Nom</th>
        <th>E-Mail</th>
        <th>Tel° </th>
        <th>Catégorie</th>
        <th>Date-Création </th>
        <th>Opération </th>

      </tr>
    </thead>
    <tbody>

        @foreach($marchands as $marchand)

        <tr>
           <td>{{ $marchand->id }}</td>
           <td>{{ $marchand->nom }}</td>
           <td>{{ $marchand->email }}</td>
           <td>{{ $marchand->tel}}</td>
           <td>{{ $marchand->categorie_id }}</td>
           <td>{{ $marchand->created_at }}</td>


           <td>
              <form action="{{action('MarchandController@destroy', $marchand['id'])}}" method="POST">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-warning" type="submit" onclick="return confirm('Are you sure? You will not be able to recover this.')">Delete</button>
                </form>
          </td>
        </tr>
        @endforeach
      @endif
    </tbody>
  </table>
  </div>
@endsection
