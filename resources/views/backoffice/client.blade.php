@extends('backoffice.master')

@section('content')
<div class="container">
    <br />
    <h2> Gestion des clients 
    </h2>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />

     @endif
       <br>
       @if($data->isEmpty())
       <div class="alert alert-warning" role="alert">
            Pas de Client !
          </div>
       @else
     <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th> Nom & Prenom</th>
        <th>Adresse</th>
        <th>Tel° </th>
        <th>E-Mail</th>
        <th>ID-Carte</th>
        <th>Date-Carte</th>
        <th>Opération </th>

      </tr>
    </thead>
    <tbody>


        @foreach($data as $row)
       <tr>
           <td>{{ $row->id }} </td>
           <td>{{ $row->nom }} {{ $row->prenom }}</td>
           <td>{{ $row->adresse}}</td>
           <td>{{ $row->tel }}</td>
           <td>{{ $row->email}}</td>
           <td>{{ $row->carte_id }}</td>
           <td>{{ $row->created_at}}</td>
           <td>
              <form action="{{action('ClientController@destroy', $row['id'])}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-warning" type="submit" onclick="return confirm('Are you sure? You will not be able to recover this.')">Delete</button>
                </form>
          </td>
        </tr>
        @endforeach
        @endif
    </tbody>
  </table>
  </div>
@endsection
