@extends('backoffice.master')

@section('content')
<div class="container">
    <br />
    <h2> Gestion des Cartes </h2>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />

     @endif
       <br>
     <table class="table table-striped">
    <thead>
      <tr>
        <th>ID-Carte</th>
        <th> Date de Validité </th>
        <th>Date d'echance </th>
        <th>Date-Création </th>
        <th> Date-Descativation </th>
        <th calspan="2">Opération </th>

      </tr>
    </thead>
    <tbody>
        @foreach($cartes as $carte)
       <tr>
           <td>{{ $carte->id }} </td>
           <td>{{ $carte->dateval }}</td>
           <td>{{ $carte->datech }}</td>
           <td>{{ $carte->created_at }}</td>
           <td>{{ $carte->deleted_at }}</td>

              <td>
                <form action="{{action('CarteController@destroy', $carte['id'])}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-info" type="submit"> Désactiver</button>
                </form>
              </td>

        </tr>
        @endforeach

    </tbody>
  </table>
  </div>
@endsection
