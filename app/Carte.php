<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Carte extends Model
{
    use SoftDeletes ;

    public function clients() {

    return $this->belongsto(Client::class);

    }
    public function privileges ()
    {
  return $this->belongsToMany(Privilege::class);
    }

    protected $fillable = [
        'dateval','datech',
    ];
    protected $dates=['deleted_at'];
}
