<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    public function marchand() // function du relation
      {
          return $this->belongsTo(Marchand::class);

      }
      public function client()
{
    return $this->belongsToMany(Client::class);
}
          public function carte()
{
      return $this->belongsToMany(Carte::class);
}
public function pointvent()
{
      return $this->belongsToMany(Pointvente::class);
}
      protected $fillable = [
        'nompriv', 'remise', 'descrip','datedep' ,'datefin'
    ];
}

