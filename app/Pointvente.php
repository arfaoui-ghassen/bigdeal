<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pointvente extends Model
{
    public function marchand()
      {
          return $this->belongsTo(Marchand::class);

      }
      public function privilege()
      {
            return $this->belongsToMany(Privilege::class);
      }
}
