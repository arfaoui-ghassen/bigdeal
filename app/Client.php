<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Client extends Model
{

    public function carte()  // les relation des base de base donneé entre les table
    {
        return $this->hasOne(Carte::class);
     }
    public function privilege() {
        $priv_list=[];
        $this->priv=(ClientPrivis::class)::select('privilege_id')->where('client_id',$this->id)->get();        
        if (is_null($this->priv)) return null;
        else{
          
          foreach ($this->priv as $key => $value) {
            $priv_list[$value->privilege_id]=$value->privilege_id;
          }
          $priv=DB::table('privileges')
                          ->whereIn('id', $priv_list)
                          ->get();    
          return($priv);     
      }
    }
      protected $fillable = [
        'nom', 'prenom', 'adresse','email' ,'date_nais','tel','codepost','carte_id'
    ];

}

