<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marchand extends Model
{
    public function categorie()
      {
          return $this->belongsTo(Categorie::class);

      }
      public function pointvente()
      {
          return $this->hasMany(Pointvente::class);

      }
      public function privilege()
      {
          return $this->hasMany(Privilege::class);

      }
        protected $fillable = [
        'nom', 'email', 'tel','motpass' ,'categorie_id'
    ];
}
