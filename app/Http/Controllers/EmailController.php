<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Mail;
class EmailController extends Controller
{
	public function SendNotification(){
		$client_list=Client::select('email')->get();
		$client_emails=[];
		foreach ($client_list as $key => $value) {
			array_push($client_emails,$value->email);
		}
	
		// $emails = ['myoneemail@esomething.com', 'myother@esomething.com','myother2@esomething.com'];

		Mail::send('backoffice.emailNotif', [], function($message) use ($client_emails)
		{    
			$message->to($client_emails)->subject('New Offres');    
		});
		return ( Mail:: failures());
		// exit;
	}
}
