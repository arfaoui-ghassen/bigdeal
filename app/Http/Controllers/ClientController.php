<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Client;
use App\Carte;
use App\Privilege;
use Carbon\carbon;
use Validator;
use Auth,Session,Redirect;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()   // afficher la liste
        {
            verifyadmin();
            $data= Client::all();
            return view('backoffice.client',compact('data'));
        }
    public function clienthome()   // afficher la liste
        {
            if(session()->has('client')){
                $client=Client::find(session()->get('client'));
                $client->privilege();
                return view('frontoffice.clientAcc')->with('client',$client);
            }else{
             return redirect()->to('/logasclient');
            }
        }

    public function loginuser(Request $request ){


        $validated =Validator::make($request->all(), [
            'carte_id'=> 'required|numeric' ,
        ]);

        if ($validated->fails()) {
            return back()->with('error','Verifier votre numéro de carte SVP  "'.$request->carte_id.'"');
        }else{
            $carte = Carte::find($request->carte_id);
            if(is_null($carte)) 
                return back()->with('error','Verifier votre numéro de carte SVP  '.$request->carte_id);
            else {
                $usr_id=$carte->clients()->getParent();
                // Auth::loginUsingId($usr_id, TRUE);
                $client=Client::where('carte_id',$usr_id->id)->first();
                if(is_null($client)){

                    return back()->with('error',' Problem avec carte number :"   '.$request->carte_id .'  " please contact admin');
                }
                else {
                $priv_list=Privilege::where('status','1')->get();
                foreach($priv_list as $key=>$value){
                    // dump($value->);
                }
                Session::put('client',$client->id);
                return view('frontoffice.clientAcc')->with('client',$client)->with('priv_list',$priv_list);
                // return $this->clienthome();  
                } 

            }
        }


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontoffice.createc');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=> 'required|max:255',
            'prenom' =>  'required|max:255',
            'adresse'=> 'required|string|max:255' ,
            'email' =>'required|string|' ,
            'date_nais'=>'required',
            'tel'=>'required',
            'codepost' =>'required|max:4',
        ]);
            $carte = new carte(
                ['dateval'=>Carbon::today()->addYear()->format('Y-m-d')]
            );
            $carte->save();

            $tosave=$request->all();
            $tosave['carte_id']=$carte->id;
            $client=Client::create($tosave);
            $client->save();
            // return Redirect::to('clientAcc')
            // ->with('success','Great! Votre ID est '.$carte->id)
            Session::put('success','Great! Votre ID est '.$carte->id);
            // Session::put('client',$client->id);
            return redirect::to('/logasclient');

    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        verifyadmin();
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client=Client::Find($id);
        if(!is_null($client)) return view ("frontoffice.editClient")->withClient($client);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=1)
    {

    }
    public function updateclient(Request $request,$id)
    {
        $client=Client::find($id);
        $client->update($request->all());
        return $this->clienthome();
    }   
    
    public function logout(){
        Session::forget('client');
        return redirect()->to('/Acceuil');
        
    }    

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        verifyadmin();
        $client = Client::find($id);
        $carte = Carte::where('id',$client->carte_id)->delete();
        $client->delete();
        return redirect('client')->with('success',' Le Client supprimer avec succes ');
    }
}
