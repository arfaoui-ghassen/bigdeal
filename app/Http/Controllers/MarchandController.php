<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Marchand;
use App\Pointvente ;
use App\ClientPrivis;
use App\User;
use App\Client;
use Hash,Auth,Redirect,Session;
// use Auth;
// use Redirect;
class MarchandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        verifyadmin();
        $marchands =Marchand::all();
        return view('backoffice.marchand',compact('marchands'));

    }
    public function login(Request $request ){
        $marchand =Marchand::where('email',$request->email)
        ->where('motpass' , $request->password)
        ->first();
        if(!is_null($marchand)) {
            Session::put('marchand',$marchand->id);
            $data = $request->session()->all();
            return $this->marchand_acceuil();
        }
        else {
            return Redirect::back()->with('errors', 'Verifier votre email & mot de pass');
        }

   } 
   public function logout(){
    
    session()->forget('marchand');
    return redirect()->to('/Acceuil');
   }
   public function logininter() {
        if(!Session()->has('marchand')){
            return view('frontoffice.Marchand');
        }
        else{
            return $this->marchand_acceuil();
        }
    }
      public function marchand_acceuil(){
        if(!Session::has('marchand')) return redirect::to('/logasmarchand');
        (Session::get('marchand')) ? $id=Session::get('marchand') : redirect::to('/logasmarchand');
        $client_list=$this->client_list($id);
        return view ('frontoffice.marchandAcc')
        // return Redirect::to('/marchandAcc');
        ->with('client_list',$client_list);


   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontoffice.createm');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=> 'required',
            'tel' =>  'required',
            'email'=> 'required' ,
            'point_vente' =>'required',
            'motpass' =>'required',
            
        ]);
        $tosave=$request->all();
        $tosave['categorie_id']=1;
        unset($tosave['_token']);
        $m=Marchand::create($tosave);
        $m->save();
        if(!empty($request->point_vente)){
            foreach ($request->point_vente as $key => $value) {
                $pointvente=new Pointvente;
                $pointvente->marchand_id= $m->id;
                $pointvente->adresse=$value;
                $pointvente->save();
            }
        }
        return redirect::to('/logasmarchand')->with('success','log into your account ');
    }
    public function ajoutacheteur($id){
        $marchand=Marchand::where('id',$id)->first();
        return view('frontoffice.Ajoutacheteur')->with('marchand',$marchand);

    }
    public function ajoutprivilege(Request $request){
        $request->validate([
            'carte_id'=> 'required',
            'created_at' =>  'required',
            'Adresse'=> 'required' ,
            'nom' =>'required',
            
        ]);
        $client=Client::where('carte_id',$request->carte_id)->first();
        if(is_null($client)){
            return Redirect::to('/marchandAcc')->with('errors','num carte invalide ');
        }else{
            $tosave=$request->all();
            unset($tosave['_token'],$tosave['carte_id'],$tosave['nom']);
            $tosave['client_id']=$client->id;
            // $clientpriv=ClientPrivis::create($tosave);
            $clientpriv=new ClientPrivis;
            $clientpriv->client_id  =$client->id;
            $clientpriv->privilege_id =$request->privilege_id;
            $clientpriv->Adresse =$request->Adresse;
            $clientpriv->save();
            return Redirect::to('/marchandAcc')->with('success','privilege de '.$client->nom.' ajouter avec succes ');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $marchand=Marchand::find($id);
        if ( !is_null($marchand) )  return view ('frontoffice.editm')->withMarchand($marchand);
        else abort(404) ; 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request )
    {
        $new_marchand=[];
        $marchand=Marchand::find(Session::get('marchand'));
        
        if(!is_null($request->point_ventes)){
            $allpoint=Pointvente::where('marchand_id',Session::get('marchand'));
            $allpoint->delete();
            foreach ($request->point_ventes as $key => $value) {
                $pointvente=new Pointvente;
                $pointvente->marchand_id= Session::get('marchand');
                $pointvente->adresse=$value;
                $pointvente->save();
            }                
        }
        (!is_null($request->nom)) ? $new_marchand['nom']=$request->nom : $marchand->nom;
        (!is_null($request->tel)) ? $new_marchand['tel']=$request->tel: $marchand->tel;
        (!is_null($request->email)) ? $new_marchand['email']=$request->email: $marchand->email;
        (!is_null($request->email)) ? $new_marchand['email']=$request->email: $marchand->email;
        (!is_null ($request->motpass)) ? $new_marchand['motpass']=$request->motpass: $marchand->motpass;
        $marchand->update($new_marchand);
        return $this->marchand_acceuil();
}


    public function client_list($id){
        $marchand=Marchand::find($id);
        $list_priv=$marchand->privilege()->getModels();
        $priv_list=[];
        if(!empty($list_priv)){
            foreach ($list_priv as $key => $value) {
                 array_push($priv_list,$value->id);
             }
        }
        // dd(array_values($priv_list));
        if(! $list_priv) return Null; 
        // $list=json_encode(array_values($priv_list));
        $client_list=ClientPrivis::whereIn('privilege_id',array_values($priv_list))->get();
        return($client_list);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        verifyadmin();
        $marchand = \App\Marchand::find($id);
        $marchand->delete();
        return redirect('marchand')->with('success',' Le marchand supprimer avec succes ');
    }

}
