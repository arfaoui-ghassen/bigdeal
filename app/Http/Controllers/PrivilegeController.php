<?php

namespace App\Http\Controllers;
use auth; 
use Illuminate\Http\Request;
use App\Privilege ; 
use App\Marchand;
use App\Client;
use Mail;
use Redirect,Session;
class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()   // affichage 
    {
        $privileges =Privilege::all();
        return view('backoffice.privilege',compact('privileges'));
    }
    public function addpriv(Request $request){
        $request->validate([
            'nompriv'=> 'required|max:255',
            'remise' =>  'required|max:3',
            'datedep'=> 'required' ,
            'datefin' =>'required' ,
        ]);
        $priv=$request->all();
        unset($priv['_token']);
        $x = new Privilege();
        $x->marchand()->associate(Session::get('marchand')) ;
        $x->nompriv=$request->nompriv;
        $x->remise =$request->remise;
        $x->descrip=$request->descrip;
        $x->datedep=$request->datedep;
        $x->datefin=$request->datefin;
        $x->save();
        return Redirect::to('/marchandAcc')->with('success','privilege ajouter avec succes ');;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function privstatus($id){
        dd($id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //this function is for active or desactive status && send newsletter it has been used so i dont have to create another route 
        $privilege=Privilege::find($id);
        if($privilege->status==0){
            $privilege->status=1;
            $this->notifyusers($privilege);  
        } 
        else $privilege->status=0;
        $privilege->update();
        return Redirect::to('/privilege');


    }   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $privilege = Privilege::find($id);
        $privilege->delete();
        return redirect('privileges')->with('success','Privilege has been  deleted');
    }
    public function notifyusers(){

        $client_list=Client::select('email')->get();
        $client_emails=[];
        foreach ($client_list as $key => $value) {
            array_push($client_emails,$value->email);
        }
    
        // $emails = ['myoneemail@esomething.com', 'myother@esomething.com','myother2@esomething.com'];

        Mail::send('backoffice.emailNotif', [], function($message) use ($client_emails)
        {    
            $message->to($client_emails)->subject('New Offres');    
        });
     //send newsletter 
     Session::put('activepriv','privilige active we will send notification to users ');          
    }
}
