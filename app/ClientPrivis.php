<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientPrivis extends Model
{
	protected $table = 'clitprivis';
    protected $fillable=['carte_id','created_at','Adresse','nom'];
}
