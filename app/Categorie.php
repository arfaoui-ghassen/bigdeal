<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function marchands()
      {
          return $this->hasMany(Marchand::class);

      }
}
